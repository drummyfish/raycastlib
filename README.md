# raycastlib

Public domain ray casting library for (not only) resource-limited computers.

If you like this, you may also like my similar project: [small3dlib](https://gitlab.com/drummyfish/small3dlib). These two libraries can very easily be combined together -- here is a proof-of-concept gif (environment rendered with raycastlib, cubes with small3dlib):

I've made a complete game with this library: [Anarch](https://gitlab.com/drummyfish/sucklessfps).

![](https://gitlab.com/drummyfish/small3dlib/raw/master/media/rcl_plus_s3l.gif)

eye-candy previews
------------------

Pokitto (32bit embedded console, 48 MHz, 36 kB RAM):

![](media/pokitto1.gif)
![](media/pokitto2.gif)
![](media/pokitto3.gif)

SDL (PC):

![](media/sdl.gif)

Arduboy (8bit Arduino console, 16 MHz, 2.5 kB RAM):

![](media/arduboy.gif)
![](media/arduboy2.gif)

terminal:

![](media/terminal.gif)

Gamebuino META (Arduino 32bit console, 48 MHz, 32 kB RAM):

![](media/gamebuino.gif)

features
--------

- Very **fast, small and efficient**.
- Uses **only integer math** (32bit). The tiny version should only require 16bit, but isn't completed.
- **No dependencies** (uses only stdint standard library), extremely portable.
- **Single header**, KISS, suckless.
- **No dynamic heap allocation**.
- Advanced rendering of **variable height floor and ceiling**.
- **Textured walls and floor**.
- **Depth information** (e.g. for fog or z-buffer rendering).
- **Camera shearing** (looking up/down).
- Camera movement with **collisions**.
- Partial support for **opening door**.
- **Pure C99**, tested to run as C++ as well.
- Optional framework **functions that handle the whole rendering**.
- Still **flexible** -- pixels are left for you to draw in any way you want.
- **Tested on many platforms**:
  - PC (little endian, 64bit GNU)
  - PowerPC emulator (big endian)
  - compilers: gcc, clang, emscripten (web browser, JavaScript transpile)
  - Arduboy (only experimental)
  - Pokitto (32bit resource-limited embedded ARM)
  - Gamebuino META (32bit resource-limited embedded ARM)
  - TODO:
    - Android
    - Windows
- **Many compile-time options** to tune the performance vs quality.
- **Well commented** and formatted code, with examples. Automatic documentation (comments + provided Doxyfile).
- Completely **free of legal restrictions**, do literally anything you want.

**NOTE**: Backwards compatibility isn't a goal of this libraray. It is meant to
be an as-is set of tools that the users is welcome to adjust for their
specific project. So new features will be preferred to keeping the same
interface.

how to use
----------

**Don't forget to compile with -O3!** This drastically improves performance.

For start take a look at the [helloWorld.c](programs/helloWorld.c) program and other examples.
For more examples see my [Pokitto demos](https://gitlab.com/drummyfish/Pokitto-Raycasting) repository.

Also see **the library code itself**, it is meant to be self-documenting -- you'll find the description of a lot of things at the start of the file.

The basic philosophy is:

- The library implements only a **rendering back-end**, it doesn't perform any drawing to the actual screen,
  hence there is no dependency on any library such as OpenGL or SDL. It just calls your front-end function
  and tells you which pixels you should write. How you do it is up to you.
- Before including the header, define `RCL_PIXEL_FUNCTION` to the name of a function you will use to
  draw pixels. It is basically a fragment/pixel shader function that the library will call. You will
  be passed info about the pixel and can decide what to do with it, so you can process it, discard it,
  or simply write it to the screen.
- Call `RCL_renderSimple` or `RCL_renderComplex` to perform the frame rendering. This will cause the
  library to start calling the `RCL_PIXEL_FUNCTION` in order to draw the frame. You can optionally write
  a similar function of your own using the more low-level functions which are also provided.
- The library gets info about the world (such as floor or ceiling height) via *array* functions
  (`RCL_ArrayFunction` type) -- functions that take *x* and *y* coordinates of a square and return given
  information. This way you are free to not only fetch the map data from an array, but also generate
  the world procedurally if that is what you want.
- Fixed point arithmetics is used as a principle, but there is no abstraction above it, everything is simply
  an integer (`RCL_Unit` type). The space is considered to be a dense grid, where each world square
  has a side length of `RCL_UNITS_PER_SQUARE` units. Numbers are normalized by this constant, so e.g.
  the sin function returns a value from `-RCL_UNITS_PER_SQUARE` to `RCL_UNITS_PER_SQUARE`.

license
-------

Everything is CC0 1.0 (public domain, https://creativecommons.org/publicdomain/zero/1.0/) + a waiver of all other IP rights (including patents).

I've written all code completely myself from scratch, the art used in demos is either my own released under CC0 or someone else's released under CC0.

This is not a mandatory condition, but please consider supporting free software and free culture by using free licenses and/or waivers.

If you'd like to support me or just read something about me and my projects, visit my site: [www.tastyfish.cz](http://www.tastyfish.cz/).

You can also choose to use this under the following waiver which is here to just ensure more legal safety and waiving of additional IP such as patents:

The intent of this waiver is to ensure that this work will never be encumbered by any exclusive intellectual property rights and will always be in the public domain world-wide, i.e. not putting any restrictions on its use.

Each contributor to this work agrees that they waive any exclusive rights, including but not limited to copyright, patents, trademark, trade dress, industrial design, plant varieties and trade secrets, to any and all ideas, concepts, processes, discoveries, improvements and inventions conceived, discovered, made, designed, researched or developed by the contributor either solely or jointly with others, which relate to this work or result from this work. Should any waiver of such right be judged legally invalid or ineffective under applicable law, the contributor hereby grants to each affected person a royalty-free, non transferable, non sublicensable, non exclusive, irrevocable and unconditional license to this right.
